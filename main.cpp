#include <M5Core2.h>

void taskHelloToSerial( void *pvParameters );
void taskBonjourToSerial( void *pvParameters );

// SETUP ******************************************************************
void setup() {
  // put your setup code here, to run once:
  M5.begin();
  Serial.updateBaudRate(1200);  // Change la vitesse du port série à 1200 BAUDS 
                                // /!\ Penser à changer la vitesse du moniteur série dans platformio.ini !

  xTaskCreate(
    taskHelloToSerial,  /* Task function. */
    "HelloToSerial",    /* name of task. */
    8192,               /* Stack size of task */
    NULL,               /* parameter of the task */
    4,                  /* priority of the task */
    NULL);              /* Task handle to keep track of created task */

  xTaskCreate(
    taskBonjourToSerial,  /* Task function. */
    "BonjourToSerial",    /* name of task. */
    8192,               /* Stack size of task */
    NULL,               /* parameter of the task */
    4,                  /* priority of the task */
    NULL);              /* Task handle to keep track of created task */

  Serial.println("END SETUP");
}

// LOOP *******************************************************************
void loop() {
  // Nothing To Do !
}

/**************************************************************************
 * Tâche qui envoie "Hello World !!!" tous les 1/20s sur le port série
*/
void taskHelloToSerial( void *pvParameters ) {
    for( ;; ) // <- boucle infinie
    {
        // Equivalent à Serial.println("Hello World !!!\n");
        char *p = "Hello World !!!\n";
        while (*p) {Serial.print(*p++);}

        delay(50);
    }
}

/**************************************************************************
 * Tâche qui envoie "Bonjour à tous !!!" tous les 1/100s sur le port série
*/
void taskBonjourToSerial( void *pvParameters ) {
    for( ;; ) // <- boucle infinie
    {
        // Equivalent à Serial.println("Bonjour à tous !!!\n");
        char *p = "Bonjour à tous !!!\n";
        while (*p) {Serial.print(*p++);}
    
        delay(10);
    }
}